#=
ToDoApp:
- Julia version: 1.9.2
- Author: ed
- Date: 2023-07-27
=#
module ToDoApp
    using Dates
    export print_todo, ToDo

    mutable struct ToDo
        id::Int32
        description::String
        completed::Bool
        created::Date
        priority::Int8
    end

    function print_todo(todo)
        if !todo.completed
            println("I still have to do: $(todo.description)")
            print("A todo created at: " )
            helper(todo)
        end
    end

    helper(todo) = println(todo.created)
end
