include("ToDoApp.jl")
using .ToDoApp, Dates

todo1 = ToDo(1, "Getting groceries", false, Date("2023-07-29", "yyyy-mm-dd"), 5)
print_todo(todo1)

ToDoApp.helper(todo1)
