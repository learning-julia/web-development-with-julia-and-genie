form = """ <form action="/" method="POST" enctype="multipart/form-data"> <input type="text"
name="name" value="" placeholder="What's your name?" /> <input type="submit" value="Greet" />
</form> """
println(form)

list = [1, 2, 3]
newList = map(x -> x^2, list)
println(list)
println(newList)
println()

sym = :info
println(sym)
println(typeof(sym))
