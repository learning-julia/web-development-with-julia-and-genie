const var = Ref{Float64}(0.0)
var[] = 20.0
println(var)

b :: Integer = 42
println(typeof(b))

using Dates
todo1 = [1, "Getting groceries", false, Date("2023-07-26", "yyyy-mm-dd"), 5]
println(todo1[2])
println(todo1[4])

mutable struct ToDo
  id::Int32
  description::String
  completed::Bool
  created::Date
  priority::Int8
end

todo2 = ToDo(1, "Getting groceries", false, Date("2023-07-26", "yyyy-mm-dd"), 5)
println(todo2.description)
todo2.completed = true
println(todo2.completed)
println()

ex = :(a + b * c + 1)
a = 1
b = 2
c = 3
println("ex is $ex")  # => ex is a + b * c + 1
println("ex is $( eval(ex) )")  # => ex is 8
