using Dates

mutable struct ToDo
  id::Int32
  description::String
  completed::Bool
  created::Date
  priority::Int8
end

todo1 = ToDo(1, "Getting groceries", false, Date("2023-07-26", "yyyy-mm-dd"), 5)
todo2 = ToDo(2, "Learn Julia", false, Date("2023-07-26", "yyyy-mm-dd"), 4)

if todo2.priority > todo1.priority
  println("Better do todo2 first")
else
  println("Better do todo1 first")
end

# functions
increase_priority!(todo) = todo.priority += 1
println(todo1.priority)
increase_priority!(todo1)
println(todo1.priority)

abstract type Vehicle end
function move(v::Vehicle, dist::Float64)
  println("Moving by $dist meters")
end
abstract type LightYears end
function move(v::Vehicle, dist::LightYears)
  println("Blazing across $dist light years")
end

# next line produces an error
increase_priority!("does this work?")
