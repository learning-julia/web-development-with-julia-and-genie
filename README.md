# Web Development with Julia and Genie

Sample code for _"Web Development with Julia and Genie"_ book from Packt Publishing.

## License

[MIT](./LICENSE)
