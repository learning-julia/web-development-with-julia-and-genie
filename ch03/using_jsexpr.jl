using JSExpr

@js function cube(arg)
    return arg * arg * arg
end

var1 = 108
callback = @js n -> n + $var1

js"var 1 is: $var1"

message = "hi"
fun1 = js"
    function() {
        alert($message)
    }
"
