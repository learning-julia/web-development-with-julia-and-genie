using HTTP

# Request
req = HTTP.Request(
    "GET",
    "http://localhost:8081/search",
    ["Content-Type" => "text/plain"],
    "Hi there!"
)

println(req.method)
println(req.target)
println(req.headers)
println(req["Content-Type"])
println(HTTP.payload(req))

println(String(HTTP.payload(req)))
println(splitpath(req.target)[2])
println()

# Response
resp = HTTP.Response(
   200,
   ["Content-Type" => "text/plain"],
   body="Hi there!"
)

println(resp.status)
println(resp.headers)
println(resp["Content-Type"])

println(String(resp.body))
println(String(HTTP.payload(resp)))
