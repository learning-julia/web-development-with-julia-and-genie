using HTTP

HTTP.serve() do request
    try
        return HTTP.Response("Still lots of ToDos!")
    catch e
        return HTTP.Response(404, "Error: $e")
    end
end
