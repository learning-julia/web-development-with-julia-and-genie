using HTTP, Sockets

todos= """
ToDo 1: Getting groceries
ToDo 2: Learn Julia and Genie
ToDo 3: Walk the dog
"""

const HOST = ip"127.0.0.1"
const PORT = 9999
const ROUTER = HTTP.Router()
HTTP.register!(ROUTER, "GET", "/*", req -> HTTP.Response(200, "Hello"))
HTTP.register!(ROUTER, "GET", "/list_todos", req -> HTTP.Response(200, todos))
HTTP.serve(ROUTER, HOST, PORT)
