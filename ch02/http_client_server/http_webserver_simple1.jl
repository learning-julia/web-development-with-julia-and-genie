using HTTP

HTTP.listen() do http
    while !eof(http)
        println("body data: ", String(readavailable(http)))
    end
    HTTP.setstatus(http, 200)
    HTTP.setheader(http, "Content-Type" => "text/html")
    HTTP.startwrite(http)
    write(http, "ToDo 1: Getting groceries<br>")
    write(http, "ToDo 2: Learn Julia<br>")
    write(http, "ToDo 3: Walk the dog")
end
