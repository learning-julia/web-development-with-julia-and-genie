using HTTP, Sockets, StructTypes, JSON3, Dates

mutable struct ToDo
    id::Int64
    description::String
    completed::Bool
    created::Date
    priority::Int8
end

const ToDos = Dict{Int,ToDo}()

function initToDos()
    todo1 = ToDo(1, "Getting groceries", false, Date("2023-08-01", "yyyy-mm-dd"), 5)
    todo2 = ToDo(2, "Learn Julia and Genie", false, Date("2023-08-02", "yyyy-mm-dd"), 4)
    todo3 = ToDo(3, "Walk the dog", true, Date("2023-07-28", "yyyy-mm-dd"), 6)
    todo4 = ToDo(4, "Paying the energy bill", false, Date("2023-08-04", "yyyy-mm-dd"), 8)
    todo5 = ToDo(5, "Blog about workspace management", true, Date("2023-07-29", "yyyy-mm-dd"), 4)
    todo6 = ToDo(6, "Book a flight to the Moon", false, Date("2023-08-04", "yyyy-mm-dd"), 3)
    todo7 = ToDo(7, "Conquer the world", true, Date("2023-07-29", "yyyy-mm-dd"), 1)
    ToDos[1] = todo1
    ToDos[2] = todo2
    ToDos[3] = todo3
    ToDos[4] = todo4
    ToDos[5] = todo5
    ToDos[6] = todo6
    ToDos[7] = todo7
end

function getToDo(req::HTTP.Request)
    todoId = HTTP.URIs.splitpath(req.target)[3]
    todoId = parse(Int64, todoId)
    if haskey(ToDos, todoId)
        todo = ToDos[todoId]
        json = JSON3.write(todo)
        return HTTP.Response(200, json)
    else
        return HTTP.Response(200, JSON3.write("No ToDo with that key exists."))
    end
end

function deleteToDo(req::HTTP.Request)
    todoId = HTTP.URIs.splitpath(req.target)[3]
    todo = ToDos[parse(Int64, todoId)]
    delete!(ToDos, todo.id)
    return HTTP.Response(200)
end

function createToDo(req::HTTP.Request)
    todo = JSON3.read(IOBuffer(HTTP.payload(req)), ToDo)
    todo.id = maximum(collect(keys(ToDos))) + 1
    ToDos[todo.id] = todo
    return HTTP.Response(200, JSON3.write(todo))
end

function updateToDo(req::HTTP.Request)
    todo = JSON3.read(IOBuffer(HTTP.payload(req)), ToDo)
    ToDos[todo.id] = todo
    return HTTP.Response(200, JSON3.write(todo))
end

#StructTypes.StructType(::Type{ToDo}) = StructTypes.Mutable()

initToDos()
const HOST = ip"127.0.0.1"
const PORT = 8080
const ROUTER = HTTP.Router()
HTTP.register!(ROUTER, "POST", "/api/todos", createToDo)
HTTP.register!(ROUTER, "GET", "/api/todos/*", getToDo)
HTTP.register!(ROUTER, "PUT", "/api/todos", updateToDo)
HTTP.register!(ROUTER, "DELETE", "/api/todos/*", deleteToDo)
HTTP.serve(ROUTER, HOST, PORT)
