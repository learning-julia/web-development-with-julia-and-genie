using HTTP, JSON3

todos = Dict( # type is Dict{Int64, String}
    1 => "Getting groceries",
    2 => "Learn Julia and Genie",
    3 => "Walk the dog"
)

json_string = JSON3.write(todos)
println(json_string)

todos2 = JSON3.read(json_string)
println(todos2)
println(todos2[1])
println()

resp = HTTP.Response(
    200,
    ["Content-Type" => "application/json"],
    body=JSON3.write(todos)
)
println(resp)
println()

body = HTTP.payload(resp)
io = IOBuffer(body)
todos = JSON3.read(io)
# or combined in one line:
todos = JSON3.read(IOBuffer(HTTP.payload(resp)))
println(todos)
