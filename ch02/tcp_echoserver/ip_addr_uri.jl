using Sockets

addr = ip"185.43.124.6"
println(typeof(addr))

connect("julialang.org", 80)
println(getaddrinfo("julialang.org"))
println()

using URIs, HTTP

req = HTTP.Messages.Request("GET", "https://www.google.com/search?q=mammoth")

uri = URI(req.target)
println(uri.scheme)
println(uri.host)
println(uri.path)
println(uri.query)
println(queryparams(uri))
