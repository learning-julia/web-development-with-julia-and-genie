using Sockets

server = listen(8080)
while true
    conn = accept(server)
    @async begin
        try
            while true
                line = readline(conn)
                println(line)
                if chomp(line) == "S"
                    println("Stopping TCP server...")
                    close(conn)
                    exit(0)
                else
                    write(conn, line)
                end
            end
        catch ex
            print("connection lost with error $ex")
            close(conn)
        end
    end # end coroutine block
end
close(conn)
