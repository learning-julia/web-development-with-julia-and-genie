using Sockets

errormonitor(@async begin
    server = listen(8080)
    while true
        conn = accept(server)
        @async while isopen(conn)
            write(conn, readline(conn, keep=true))
        end
    end
end)
sleep(1) # give the server time to start

client = connect(8080)
errormonitor(@async while isopen(client)
    write(stdout, readline(client, keep=true))
end)
println(client, "Hello World from the Echo Server")
sleep(1) # give the server time to respond
close(client)
