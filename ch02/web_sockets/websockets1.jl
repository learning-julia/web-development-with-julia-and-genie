using WebSockets

function main()
    WebSockets.open("wss://wsaws.okex.com:8443/ws/v5/public") do ws
        for i = 1:100
            a = time_ns()
            write(ws, "ping")
            data, success = readguarded(ws)
            !success && break
            b = time_ns()
            println(String(data), " ", (b - a) / 1000000)
            sleep(0.1)
        end
    end
end

main()
